terraform {
  backend "s3" {
    bucket = "recipe-app-api-devops-tfstate-mhs"
    key    = "recipe-app.tfstate"
    region = "us-east-1"
    encrypt = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region = "us-east-1"
  # it is best to lock the version of the provider to avoid breaking changes
  version = "~> 2.54.0"
}