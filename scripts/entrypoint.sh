#!/bin/sh
# to make the file executable, run the above command

# Exit immediately if a command exits with an error or a non-zero status.
set -e

# Run the command with the specified arguments.
python manage.py collectstatic --noinput
# python manage.py makemigrations
python manage.py wait_for_db
# python manage.py migrate --noinput
python manage.py migrate
# python manage.py runserver
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
